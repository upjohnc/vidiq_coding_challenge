from datetime import datetime

from airflow.contrib.operators.aws_athena_operator import AWSAthenaOperator
from airflow.models import DAG

from partition_add import get_partition_string
from utils import get_table_info

database, table_name, _ = get_table_info()

with DAG(dag_id='vidiq_partition_add',
         schedule_interval=None,
         start_date=datetime(2019, 12, 1)):
    add_partition = AWSAthenaOperator(
        # assume that IAM credentials stored in a `Connection` in airflow
        task_id='add_partition',
        query=get_partition_string(date_=datetime.utcnow(), database=database, table_name=table_name),
        output_location='s3://vidiq-s3/data/',
        database=f'{database}'
    )
