from pathlib import Path

import pyarrow.parquet as pq
import yaml

present_dir = Path(__file__).parent.resolve()
root_dir = present_dir.parent.resolve()

with open(present_dir / 'table_constants.yaml') as f:
    constants = yaml.load(f, Loader=yaml.FullLoader)
database = constants['database']
table_name = constants['table_name']

parquet_data = pq.read_table(root_dir / 'data' / 'part.parquet').to_pandas()

columns = [f'`{column_name}` {type_}' for column_name, type_ in parquet_data.dtypes.items()]
# remove `event_time` to make it a partition
columns_override = ',\n'.join([item for item in columns if 'event_time' not in item])
columns_converted = columns_override.replace('object', 'STRING').replace('int64', 'INT').replace('bool', 'BOOLEAN')

s3_location = 's3://vidiq-s3/data/'

create_table_sql = f"""CREATE EXTERNAL TABLE IF NOT EXISTS {database}.{table_name} ({columns_converted})
    PARTITIONED BY (event_time DATE)
    STORED AS PARQUET
    LOCATION '{s3_location}'
    tblproperties ("parquet.compress"="SNAPPY");"""

with open(root_dir / 'athena_sql' / 'create_table.sql', 'w') as f:
    f.write(create_table_sql)
