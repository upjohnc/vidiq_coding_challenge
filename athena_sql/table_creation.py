from datetime import datetime
from pathlib import Path

from pyathena import connect

from partition_add import get_partition_string
from utils import get_table_info

present_dir = Path(__file__).parent.resolve()
root_dir = present_dir.parent

database, table_name, connection_info = get_table_info()

with open(root_dir / 'athena_sql' / 'create_table.sql') as f:
    create_table_sql = f.read()

with connect(**connection_info) as connection:
    cursor_ = connection.cursor()

    cursor_.execute(f"DROP TABLE IF EXISTS {database}.{table_name};")

    cursor_.execute(create_table_sql)

    partition_sql = get_partition_string(datetime(2019, 10, 29), database, table_name)
    cursor_.execute(partition_sql)

    cursor_.close()
