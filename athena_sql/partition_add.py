from datetime import datetime

import click
from dateutil.parser import parse
from pyathena import connect

from utils import get_table_info


def get_partition_string(date_: datetime, database: str, table_name: str):
    return f"ALTER TABLE {database}.{table_name} ADD PARTITION (event_time = '{date_.strftime('%Y-%m-%d')}');"


@click.command()
@click.argument('date_')
def main(date_):
    try:
        date_ = parse(date_)
    except (ValueError, OverflowError):
        raise

    database, table_name, connection_parameters = get_table_info()
    partition_string = get_partition_string(date_=date_, database=database, table_name=table_name)
    with connect(**connection_parameters) as connection:
        cursor_ = connection.cursor()
        cursor_.execute(partition_string)
        cursor_.close()


if __name__ == '__main__':
    main()
