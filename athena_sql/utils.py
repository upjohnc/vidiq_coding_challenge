import os
from pathlib import Path

import yaml


def get_table_info():
    root_dir = Path(__file__).parent.parent.resolve()
    with open(root_dir / 'athena_sql' / 'table_constants.yaml') as f:
        constants = yaml.load(f, Loader=yaml.FullLoader)
    database = constants['database']
    table_name = constants['table_name']
    connection_info = {
        'aws_access_key_id': os.environ['AWS_ACCESS_KEY_ID'],
        'aws_secret_access_key': os.environ['AWS_SECRET_ACCESS_KEY'],
        's3_staging_dir': 's3://vidiq-s3/data/',
        'region_name': 'us-east-1',
    }
    return database, table_name, connection_info
