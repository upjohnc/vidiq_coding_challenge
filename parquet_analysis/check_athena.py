import os

from pyathena import connect

connection_info = {
    'aws_access_key_id': os.environ['AWS_ACCESS_KEY_ID'],
    'aws_secret_access_key': os.environ['AWS_SECRET_ACCESS_KEY'],
    's3_staging_dir': 's3://vidiq/data/',
    'region_name': 'us-east-1',
}
with connect(**connection_info) as connection:
    cursor_ = connection.cursor()
    cursor_.execute("SELECT count(*) FROM vidiq.parquet_vidiq")
    print(cursor_.description)
    print(cursor_.fetchall())
