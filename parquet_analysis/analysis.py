from pathlib import Path

import pyarrow.parquet as pq

schema = 'vidiq'
table_name = 'parquet_vidiq'


present_dir = Path(__file__).parent.resolve()
root_dir = present_dir.parent.resolve()

parquet_data = pq.read_table(root_dir / 'data' / 'part.parquet').to_pandas()
print(parquet_data.columns)
print(parquet_data['event_time'].head())

# columns = ',\n'.join([f'`{column_name}` {type_}' for column_name, type_ in parquet_data.dtypes.items()])
# columns_converted = columns.replace('object', 'STRING').replace('int64', 'INT').replace('bool', 'BOOLEAN')
#
# s3_location = 's3://vidiq-s3/data/'
#
# create_table_sql = f"""CREATE EXTERNAL TABLE IF NOT EXISTS {schema}.{table_name} ({columns_converted})
#     STORED AS PARQUET
#     LOCATION '{s3_location}'
#     tblproperties ("parquet.compress"="SNAPPY");"""
#
# with open(root_dir / 'athena_sql' / 'create_table.sql', 'w') as f:
#     f.write(create_table_sql)
