"""
This script is to test out the steps in creating and terminating an EMR cluster in AWS
The Dag will replicate these steps to run in Airflow

Steps :Implement workflow which consists of 3 steps in a sequence (operators/tasks)
- spins up a simple EMR cluster (1 Master, 1 Core)
- waits until created cluster is terminated
- prints DONE to log/console/…
"""
import os

import boto3
from botocore.exceptions import WaiterError

connection = boto3.client(
    'emr',
    region_name='us-east-1',
    aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'],
)

cluster_name = 'test_emr_job_boto3'
job_flow = connection.run_job_flow(
    Name=cluster_name,
    LogUri='s3://vidiq-s3/emr_logs/',
    ReleaseLabel='emr-5.18.0',
    Applications=[
        {
            'Name': 'Spark'
        },
    ],
    Instances={
        'InstanceGroups': [
            {
                'Name': "Master nodes",
                'Market': 'ON_DEMAND',
                'InstanceRole': 'MASTER',
                'InstanceType': 'm1.xlarge',
                'InstanceCount': 1,
            },
            {
                'Name': "Slave nodes",
                'Market': 'ON_DEMAND',
                'InstanceRole': 'CORE',
                'InstanceType': 'm1.xlarge',
                'InstanceCount': 1,
            }
        ],
        'Ec2KeyName': 'mykeypair',
        'KeepJobFlowAliveWhenNoSteps': True,
        'TerminationProtected': False,
        'Ec2SubnetId': 'subnet-cb1c10ac',
    },
    VisibleToAllUsers=True,
    JobFlowRole='EMR_EC2_DefaultRole',
    ServiceRole='EMR_DefaultRole',
)

cluster_id = job_flow['JobFlowId']
print(f'cluster created with the id {cluster_id}')

waiter_running = connection.get_waiter('cluster_running')
delay = 60
max_attempts = 60
try:
    waiter_running.wait(
        ClusterId=cluster_id,
        WaiterConfig={
            'Delay': delay,
            'MaxAttempts': max_attempts,
        }
    )
except WaiterError:
    print(f'cluster did not start after {delay * max_attempts} seconds')

response = connection.terminate_job_flows(JobFlowIds=[cluster_id])
waiter_terminated = connection.get_waiter('cluster_terminated')
try:
    waiter_terminated.wait(
        ClusterId=cluster_id,
        WaiterConfig={
            'Delay': delay,
            'MaxAttempts': max_attempts,
        }
    )
except WaiterError:
    print(f'cluster failed to terminate after {delay * max_attempts} seconds')

print('DONE')
