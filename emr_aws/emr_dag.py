"""
Dag for creating and terminating an EMR cluster
Assumption: the `aws` and `emr` credentials are stored in the connections section of Airflow
"""
import logging
from datetime import datetime

from airflow.contrib.operators.emr_create_job_flow_operator import \
    EmrCreateJobFlowOperator
from airflow.contrib.operators.emr_terminate_job_flow_operator import \
    EmrTerminateJobFlowOperator
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator


def log_complete():
    logging.info('DONE')


with DAG(
        dag_id='vidiq_emr_task',
        schedule_interval=None,
        start_date=datetime(2019, 12, 1)
) as dag:
    cluster_creator = EmrCreateJobFlowOperator(
        task_id='create_job_flow',
        aws_conn_id='aws_default',
        emr_conn_id='emr_default'
    )

    cluster_remover = EmrTerminateJobFlowOperator(
        task_id='remove_cluster',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='create_job_flow', key='return_value') }}",
        aws_conn_id='aws_default'
    )

    complete = PythonOperator(
        task_id='log_done',
        python_callable=log_complete,
    )

    cluster_creator >> cluster_remover >> complete
