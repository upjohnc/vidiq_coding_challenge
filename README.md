# VidIQ Coding Challenge

Basics: take a parquet file and load into an Athena table


## Notes on Partitioning
- added a partition dag.  It uses the aws operator for Athena.
- `event_time` is set as a partition in the table creation query
- a partition is added for `2019-10-29`

### Hurdle with Partitioning:
- I went through the stages of learning something new
    - starting from no knowledge, pull up the docs with, hopefully, a tutorial
    - try out the tutorial and noting the patterns in how things work
    - reverse engineer the tutorial into the task that you are working on
    - run and see what happens
    - based on the result of the test run, google to find solutions from people who have hit the same problem
    - when the `googling` doesn't result in a solution, it is time to reach out to people in the community.  For me, it is people I work with and people in my community (the Nashville python community)
- The hurdle that I was hitting was that I didn't understand that the prefixes in the s3 bucket need to directly relate to the defined partition
    - talked to a friend in the community who explained this requirement of partitions
    - my mental hurdle was on having multiple partitions.
        - multiple partitions would mean having the permutations of sub-prefixes in the bucket
    - my friend recommended a package called `awswrangler` which would take a pandas dataframe and save to parquet files with the data partitioned (set into prefixes)
        - [awswrangler](https://aws-data-wrangler.readthedocs.io/en/latest/installation.html)
        - I went down the path of playing with it but hit the hurdle of a sub-package named `tenacity` that had differing version requirements from pyarrow and apache-airflow and awswrangler.
        - I put this attempt to get `awswrangler` on hold so that I could work on the `Good to have` portion of the tasks.
            - After doing the other tasks, I am going to try out switching from `pyarrow` to `fastparquet` to possibly get rid of the `tenacity` conflict and try out `awswrangler`

## Usage
- store the parquet file in the bucket under the prefix `vidiq-s3/data/`
- from root dir or from `athena_sql`
- run: define_table.py
    - this will create the sql statement for creating the athena table
- run: table_creation.py
    - this will drop the table if exists and create the table in athena

## Implementation Questions
- small files vs large files
    - the basics:
        - i/o will slow down small files (many files) because it would require reading many files.
        - large files can take time to collect the data.  meaning if it is streaming then it will take time to get the data in for it to be saved.  for instance a day's worth of data to save into a file would not be available for querying until the day is over.
- optimal partition
    - not sure what is expected with the question
    - the basics of partitioning is to allow presto to not have to touch all the files
    - assuming that streaming data is the source then partitioning on event date will most likely be used in the `where` filter.
    - it seems from the documentation that parquet files need to have partitions added manually rather than Athena such as for Dec 1, 2019.

## EMR Cluster Creation
- I am new to AWS's EMR solution, like I am to Athena.  I enjoyed the challenge as it was a good step into learning the AWS ecosystem.
- I started by creating a script in the directory `emr_aws` to test out the right commands to call for
    - I reverse engineered the script from this [blog site](https://medium.com/@kulasangar/create-an-emr-cluster-and-submit-a-job-using-boto3-c34134ef68a0)
    - I know that airflow aws operators use `boto3` in their classes which is the reason that I wrote the script with `boto3`
- It does the basics of creating a cluster and then terminating the cluster as described in the `Good to have` section
- Note: I do not know much about subnets so I simply took one that was created from creating a cluster through the AWS console
- I did not test the dag out in Airflow because I did not take the time to write up a `docker-compose.yml` file to test it out locally

-------
## Instructions from VidIQ
Here is an instruction:
Given
*.parquet file - with cohorts (group of events) from Amplitude


Must
1. implement python function to create Athena table, which gives access to the records (events) inside the given file (file should be saved in S3)
- think about server side encryption
- think about pros and cons of storing/querying a big number of small files vs small number of large files
- think about optimal way to partition the data, describe them and compare
2. implement python function which can add new partitions based on the event's year/month/day (to optimise queries over Athena)

Requirement
- querying count(*) over created table must not take over 2s


Shall
1. Implement Airflow workflow to add new partitions over daily incoming events

Requirement
- use airflow.contrib native AWS operators to achieve this


Good to have
1. Implement workflow which consists of 3 steps in a sequence (operators/tasks) (next runs when previous completed successfully):
- spins up a simple EMR cluster (1 Master, 1 Core)
- waits until created cluster is terminated
- prints DONE to log/console/…

Requirement
- use airflow.contrib native AWS operators to achieve this
